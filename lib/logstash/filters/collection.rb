# encoding: utf-8
require "logstash/filters/base"

class LogStash::Filters::Collection < LogStash::Filters::Base

  # Setting the config_name here is required. This is how you
  # configure this filter from your Logstash config.
  config_name "collection"

  config :each, :validate => :string, :required => false, :default => '_'
  config :target, :validate => :string, :required => false

  config :at, :validate => :hash
  config :compact, :validate => :string
  config :concat, :validate => :array
  config :delete_at, :validate => :hash
  config :drop, :validate => :hash
  config :first, :validate => :hash
  config :flatten, :validate => :string
  config :group_by, :validate => :hash
  config :last, :validate => :hash
  config :map, :validate => :hash
  config :reverse, :validate => :string
  config :remove, :validate => :hash
  config :select, :validate => :hash
  config :split, :validate => :hash
  config :uniq, :validate => :string

  # Other future functions?
  # sort
  # union
  # intersect
  # zip
  # join
  # shuffle
  # take_while

  config :subfilter, :validate => :hash

  config :tag_on_failure, :validate => :string, :default => '_collection_error'

  public
  def register
  end # def register

  public
  def filter(event)
    at(event) if @at
    compact(event) if @compact
    concat(event) if @concat
    delete_at(event) if @delete_at
    drop(event) if @drop
    first(event) if @first
    flatten(event) if @flatten
    group_by(event) if @group_by
    last(event) if @last
    map(event) if @map
    reverse(event) if @reverse
    remove(event) if @remove
    select(event) if @select
    split(event) if @split
    uniq(event) if @uniq

    subfilter(event) if @subfilter

    # filter_matched should go in the last line of our successful code
    filter_matched(event)
  rescue => e
    meta = { :exception => e.message }
    meta[:backtrace] = e.backtrace if logger.debug?
    logger.warn('Exception caught while applying collection filter', meta)
    event.tag(@tag_on_failure)
  end # def filter

  private

  def at(event)
    @at.each do |field, i|
      target = target_field(event, field)
      collection = get_as_collection(event, field)
      event.set(target, collection.at(i.to_i))
    end
  end

  def compact(event)
    target = target_field(event, @compact)
    event.set(target, get_as_collection(event, @compact).compact)
  end

  def concat(event)
    @concat.each do |field, c|
      target = target_field(event, field)
      new_collection = if c.kind_of?(String)
        str_c = event.sprintf(c)
	event.include?(str_c) ? get_as_collection(event, str_c) : []
      elsif c.kind_of?(Array)
        c.map { |_| event.sprintf(_) }.select { |_| event.include?(_) }.map { |_| get_as_collection(event, _) }.reduce([], :concat)
      else
	[c]
      end

      original_collection = get_as_collection(event, field)
      event.set(target, original_collection.concat(new_collection))
    end
  end

  def delete_at(event)
    @delete_at.each do |field, i|
      target = target_field(event, field)
      c = get_as_collection(event, field)
      c.delete_at(i.to_i) # delete_at returns /removed/ values, so must mutate in place
      event.set(target, c)
    end
  end

  def drop(event)
    @drop.each do |field, i|
      target = target_field(event, field)
      event.set(target, get_as_collection(event, field).drop(i.to_i))
    end
  end

  def first(event)
    @first.each do |field, i|
      target = target_field(event, field)
      collection = get_as_collection(event, field)
      event.set(target, collection.first(i.to_i))
    end
  end

  def group_by(event)
    @group_by.each do |field, fn|
      target = target_field(event, field)
      c = get_as_collection(event, field)
      sprintf_fn = event.sprintf(fn)
      event.set(target, eval("#{c}.group_by{ |#{@each}| #{sprintf_fn}}"))
    rescue SyntaxError => syn
      raise "SyntaxError in group_by: " + syn.to_s
    end
  end

  def flatten(event)
    target = target_field(event, @flatten)
    event.set(target, get_as_collection(event, @flatten).flatten)
  end

  def last(event)
    @last.each do |field, i|
      target = target_field(event, field)
      event.set(target, get_as_collection(event, field).last(i.to_i))
    end
  end

  def map(event)
    @map.each do |field, fn|
      target = target_field(event, field)
      c = get_as_collection(event, field)
      sprintf_fn = event.sprintf(fn)
      event.set(target, eval("#{c}.map{ |#{@each}| #{sprintf_fn}}"))
    rescue SyntaxError => syn
      raise "SyntaxError in map: " + syn.to_s
    end
  end

  def reverse(event)
    target = target_field(event, @reverse)
    event.set(target, get_as_collection(event, @reverse).reverse)
  end

  def remove(event)
    @remove.each do |field, fn|
      target = target_field(event, field)
      c = get_as_collection(event, field)
      sprintf_fn = event.sprintf(fn)
      event.set(target, eval("#{c}.delete_if{ |#{@each}| #{sprintf_fn}}"))
    rescue SyntaxError => syn
      raise "SyntaxError in remove: " + syn.to_s
    end
  end

  def select(event)
    @select.each do |field, fn|
      target = target_field(event, field)
      c = get_as_collection(event, field)
      sprintf_fn = event.sprintf(fn)
      event.set(target, eval("#{c}.select{ |#{@each}| #{sprintf_fn}}"))
    rescue SyntaxError => syn
      raise "SyntaxError in select: " + syn.to_s
    end
  end

  def split(event)
    @split.each do |field, delim|
      target = target_field(event, field)
      f = event.sprintf(field)
      s = event.include?(f) ? event.get(f) : f
      event.set(target, s.split(delim))
    end
  end

  def uniq(event)
    target = target_field(event, @uniq)
    event.set(target, get_as_collection(event, @uniq).uniq)
  end


  def subfilter(event)
    # This is a powerful (and desired) capability of using collections, but arguably too
    # brittle.  Time will tell how it does.
    @subfilter.each do |field, filter|
      collection = get_as_collection(event, field)
      filter.each do |name, args|
        collection.each { |e| event.set("#{@each}", e);
	  begin
            eval(%{require "logstash/filters/#{package_name(name)}"})
            eval(%{f = LogStash::Filters::#{class_name(name)}.new(#{event.sprintf(args)}); f.register; f.filter(event)})
	  rescue SyntaxError => syn
	    raise "SyntaxError in subfilter: " + syn.to_s
	  end
        }
      end
    end
  end


  def target_field(event, field)
    t = @target.nil? ? field : @target
    event.sprintf(t)
  end

  def get_as_collection(event, field)
    f = event.sprintf(field)
    if event.include?(f)
      v = event.get(f)
      v.nil? ? [] : v.kind_of?(Array) ? v : [v]
    else
      []
    end
  end

  def package_name(name)
    name
  end

  def class_name(name)
    # Broadly speaking, the relationship between the filter name as used in the Logstash DSL
    # and the filter name as used in the import packate is simple.  Snake case "filter_name"
    # in DSL almost always can be referred to in camel case "FilterName".  Published exceptions
    # are listed below in an explicit map.
    #
    # I couldn't think of a more graceful way to handle it, except possibly offering to let the
    # author specify them in the filter declaraction - which would be more extensible, as they
    # could use it on their own custom filters, but breaks the abstraction between the DSL and
    # underlying ruby ecosystem.
    classname_map = {
      "cidr" => "CIDR",
      "csv" => "CSV",
      "de_dot" => "De_dot",
      "dns" => "DNS",
      "extractnumbers" => "ExtractNumbers",
      "geoip" => "GeoIP",
      "json_encode" => "JSONEncode",
      "kv" => "KV",
      "syslog_pri" => "Syslog_pri",
      "threats_classifier" => "Threats_Classifier",
      "useragent" => "UserAgent"
    }

    if classname_map.key?(name)
      classname_map[name]
    else
      name.split('_').map { |s| s.capitalize!}.join('')
    end
  end
end # class LogStash::Filters::Collection
