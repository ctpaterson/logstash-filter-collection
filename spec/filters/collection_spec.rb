# encoding: utf-8
require_relative '../spec_helper'
require "logstash/filters/collection"

describe LogStash::Filters::Collection do

  # === at ===
  context "at: the collection should be replaced with the value at an index" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { at => { "src" => 2 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq 'C'
    end
  end

  context "at: negative index values will count from end of collection" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { at => { "src" => -2 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq 'D'
    end
  end

  context "at: the collection should be replaced with the value at an index, nested collections are their own items" do
    event = { "src" => [ 'A', [ 'B', [ 'B1', 'B2' ] ], [ 'C1', 'C2' ], 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { at => { "src" => 2 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'C1', 'C2' ]
    end
  end

  context "at: an out-of-bounds index should replace the collection with nil" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { at => { "src" => 5 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq nil
    end
  end

  context "at: index 0 when the value in the source value is not a collection should give the source value" do
    event = { "src" => "ABCDE" }
    config <<-CONFIG
      filter {
        collection { at => { "src" => 0 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq 'ABCDE'
    end
  end

  context "at: index !0 when the value in the source field is not a collection should give nil" do
    event = { "src" => "ABCDE" }
    config <<-CONFIG
      filter {
        collection { at => { "src" => 2 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq nil
    end
  end

  context "at: the value at an index should be copied to the field named by target" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { at => { "src" => 2 } target => "dst" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
      expect(subject.get("dst")).to eq 'C'
    end
  end

  context "at: support dereferencing of field names" do
    event = {
      "src" => [ 'A', 'B', 'C', 'D', 'E' ],
      "deref" => "src",
      "target" => "dst"
    }
    config <<-CONFIG
      filter {
        collection { at => { "%{deref}" => 2 } target => "%{target}" }
      }
    CONFIG

    sample event do
      expect(subject.get("dst")).to eq 'C'
    end
  end

  # === compact ===
  context "compact: only nils are removed from source collection" do
    event = { "src" => [ 'A', nil, 'B', '', 'C', 0, 'D', [], 'E' ] }

    config <<-CONFIG
      filter {
        collection { compact => "src" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', '', 'C', 0, 'D', [], 'E' ]
    end
  end

  context "compact: all nils are removed from source collection" do
    event = { "src" => [ nil, nil, nil ] }

    config <<-CONFIG
      filter {
        collection { compact => "src" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq []
    end
  end

  context "compact: compacting a non-collection value will result in collection" do
    event = { "src" => 'ABCDE' }

    config <<-CONFIG
      filter {
        collection { compact => "src" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'ABCDE' ]
    end
  end

  context "compact: the compacted collection is written to the target field" do
    event = { "src" => [ 'A', nil, 'B', '', 'C', 0, 'D', [], 'E' ] }

    config <<-CONFIG
      filter {
        collection { compact => "src" target => "dst" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', nil, 'B', '', 'C', 0, 'D', [], 'E' ]
      expect(subject.get("dst")).to eq [ 'A', 'B', '', 'C', 0, 'D', [], 'E' ]
    end
  end

  context "compact: the fieldnames can be dereferenced" do
    event = {
      "src" => [ 'A', nil, 'B', '', 'C', 0, 'D', [], 'E' ],
      "deref" => "src",
      "target" => "dst"
    }

    config <<-CONFIG
      filter {
        collection { compact => "%{deref}" target => "%{target}" }
      }
    CONFIG

    sample event do
      expect(subject.get("dst")).to eq [ 'A', 'B', '', 'C', 0, 'D', [], 'E' ]
    end
  end

  # === concat ===
  context "concat: concatening two collections will result in one collection" do
    event = {
      "src" => [ 'A', 'B', 'C' ],
      "2nd" => [ 'D', 'E' ]
    }

    config <<-CONFIG
      filter {
        collection { concat => { "src" => "2nd" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
    end
  end

  context "concat: duplicates will not be managed" do
    event = {
      "src" => [ 'A', 'B', 'C' ],
      "2nd" => [ 'C', 'D', 'E' ]
    }

    config <<-CONFIG
      filter {
        collection { concat => { "src" => "2nd" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'C', 'D', 'E' ]
    end
  end

  context "concat: non-collection values will still be appended" do
    event = {
      "src" => [ 'A', 'B' ],
      "2nd" => 'C'
    }

    config <<-CONFIG
      filter {
        collection { concat => { "src" => "2nd" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C' ]
    end
  end

  context "concat: non-collection value in source field will be converted" do
    event = {
      "src" => 'A',
      "2nd" => [ 'B', 'C' ]
    }

    config <<-CONFIG
      filter {
        collection { concat => { "src" => "2nd" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C' ]
    end
  end

  context "concat: concating non-existent fields is harmless" do
    event = {
      "src" => [ 'A', 'B', 'C' ]
    }

    config <<-CONFIG
      filter {
        collection { concat => { "src" => "2nd" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C' ]
    end
  end

  context "concat: concating to non-existent field is harmless" do
    event = {
      "2nd" => [ 'A', 'B', 'C' ]
    }

    config <<-CONFIG
      filter {
        collection { concat => { "src" => "2nd" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C' ]
    end
  end

  context "concat: multiple fields are concatenated to the target collection" do
    event = {
      "src" => [ 'A', 'B', 'C' ],
      "2nd" => 0,
      "3rd" => [ "+", "-", "*", "/" ]
    }

    config <<-CONFIG
      filter {
        collection { concat => { "src" => [ "2nd", "3rd" ] } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 0, '+', '-', '*', '/' ]
    end
  end

  context "concat: concatenated collections are written to the target field" do
    event = {
      "src" => [ 'A', 'B', 'C' ],
      "2nd" => [ 'D', 'E' ]
    }

    config <<-CONFIG
      filter {
        collection { concat => { "src" => "2nd" } target => "dst" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C' ]
      expect(subject.get("2nd")).to eq [ 'D', 'E' ]
      expect(subject.get("dst")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
    end
  end

  context "concat: support for dereferencing field name" do
    event = {
      "src" => [ 'A', 'B', 'C' ],
      "2nd" => [ 'D', 'E' ],
      "deref" => "src",
      "deref2" => "2nd"
    }

    config <<-CONFIG
      filter {
        collection { concat => { "%{deref}" => "%{deref2}" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
    end
  end

  # === delete_at ===
  context "delete_at: remove value at index" do
    event = {
      "src" => [ 'A', 'B', 'C', 'D', 'E' ]
    }

    config <<-CONFIG
      filter {
        collection { delete_at => { "src" => 2 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'D', 'E' ]
    end
  end

  context "delete_at: negative index values will count from end of collection" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { delete_at => { "src" => -2 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'E' ]
    end
  end

  context "delete_at: the collection should be removed with the value at an index, nested collections are their own items" do
    event = { "src" => [ 'A', [ 'B', [ 'B1', 'B2' ] ], [ 'C1', 'C2' ], 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { delete_at => { "src" => 2 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', [ 'B', [ 'B1', 'B2' ] ], 'D', 'E' ]
    end
  end

  context "delete_at: an out-of-bounds index should not affect the collection" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { delete_at => { "src" => 5 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
    end
  end

  context "delete_at: a 0 index for a non-collection value should result in empty collection" do
    event = { "src" => "ABCDE" }
    config <<-CONFIG
      filter {
        collection { delete_at => { "src" => 0 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq []
    end
  end

  context "delete_at: a !0 index for a non-collection value will result with the value in a collection" do
    event = { "src" => "ABCDE" }
    config <<-CONFIG
      filter {
        collection { delete_at => { "src" => 2 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'ABCDE' ]
    end
  end

  context "delete_at: the remaining values in the collection should be copied to the field named by target" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { delete_at => { "src" => 2 } target => "dst" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
      expect(subject.get("dst")).to eq [ 'A', 'B', 'D', 'E' ]
    end
  end

  context "delete_at: dereferencing field names" do
    event = {
      "src" => [ 'A', 'B', 'C', 'D', 'E' ],
      "deref" => "src",
      "target" => "dst"
    }
    config <<-CONFIG
      filter {
        collection { delete_at => { "%{deref}" => 2 } target => "%{target}" }
      }
    CONFIG

    sample event do
      expect(subject.get("dst")).to eq [ 'A', 'B', 'D', 'E' ]
    end
  end


  # === drop ===
  context "drop: remove values from the start of the collection" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { drop => { "src" => 2 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'C', 'D', 'E' ]
    end
  end

  context "drop: negative drop values don't remove anything" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { drop => { "src" => -2 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
    end
  end

  context "drop: remove all values from the collection" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { drop => { "src" => 6 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq []
    end
  end

  context "drop: the value in the source field is not a collection" do
    event = { "src" => "ABCDE" }
    config <<-CONFIG
      filter {
        collection { drop => { "src" => 2 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq []
    end
  end

  context "drop: write remaining collection to target field" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { drop => { "src" => 2 } target => "dst" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
      expect(subject.get("dst")).to eq [ 'C', 'D', 'E' ]
    end
  end

  context "drop: dereference field names" do
    event = {
      "src" => [ 'A', 'B', 'C', 'D', 'E' ],
      "deref" => "src",
      "target" => "dst"
    }

    config <<-CONFIG
      filter {
        collection { drop => { "%{deref}" => 2 } target => "%{target}" }
      }
    CONFIG

    sample event do
      expect(subject.get("dst")).to eq [ 'C', 'D', 'E' ]
    end
  end

  # === first ===
  context "first: first 0 items from the collection" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { first => { "src" => 0 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq []
    end
  end

  context "first: first n items from the collection" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { first => { "src" => 3 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C' ]
    end
  end

  context "first: all items from the collection" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { first => { "src" => 9 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
    end
  end

  context "first: the value in the source field is not a collection" do
    event = { "src" => "ABCDE" }
    config <<-CONFIG
      filter {
        collection { first => { "src" => 2 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'ABCDE' ]
    end
  end

  context "first: selected items from collection should be written to target field" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { first => { "src" => 3 } target => "dst" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
      expect(subject.get("dst")).to eq [ 'A', 'B', 'C' ]
    end
  end

  context "first: dereference field names" do
    event = {
      "src" => [ 'A', 'B', 'C', 'D', 'E' ],
      "deref" => "src",
      "target" => "dst"
    }

    config <<-CONFIG
      filter {
        collection { first => { "%{deref}" => 3 } target => "%{target}" }
      }
    CONFIG

    sample event do
      expect(subject.get("dst")).to eq [ 'A', 'B', 'C' ]
    end
  end

  # === flatten ===
  context "flatten: flatten all levels of nested collection" do
    event = { "src" => [ 'A', [ 'B', [ 'C', [ 'D', 'E' ] ] ] ] }
    config <<-CONFIG
      filter {
        collection { flatten => "src" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
    end
  end

  context "flatten: flattening will discard empty collections, but keep collections with nil" do
    event = { "src" => [ 'A', [], 'B', [ nil ], 'C' ] }
    config <<-CONFIG
      filter {
        collection { flatten => "src" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', nil, 'C' ]
    end
  end

  context "flatten: non-collection value will be put in a collection" do
    event = { "src" => 'ABCDE' }
    config <<-CONFIG
      filter {
        collection { flatten => "src" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'ABCDE' ]
    end
  end

  context "flatten: flatten all levels of nested collection" do
    event = { "src" => [ 'A', [ 'B', [ 'C', [ 'D', 'E' ] ] ] ] }
    config <<-CONFIG
      filter {
        collection { flatten => "src" target => "dst" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', [ 'B', [ 'C', [ 'D', 'E' ] ] ] ]
      expect(subject.get("dst")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
    end
  end

  context "flatten: dereference field names" do
    event = {
      "src" => [ 'A', [ 'B', [ 'C', [ 'D', 'E' ] ] ] ],
      "deref" => "src",
      "target" => "dst"
    }
    config <<-CONFIG
      filter {
        collection { flatten => "%{deref}" target => "%{target}" }
      }
    CONFIG

    sample event do
      expect(subject.get("dst")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
    end
  end

  # === group_by ===
  context "group_by: group simple values together" do
    event = { "src" => [ 'A', 'B', 'C', 'A', 'B', 'A' ] }
    config <<-CONFIG
      filter {
        collection { group_by => { "src" => "_" } }
      }
    CONFIG

    sample event do
      insist { subject.get("src") } === { "A" => [ "A", "A", "A" ],
                                          "B" => [ "B", "B" ],
					  "C" => [ "C" ] }
    end
  end

  context "group_by: group hash values together" do
    event = { "src" => [ { "A" => "1", "B" => "2" },
			 { "A" => "2", "B" => "4" },
                         { "A" => "1", "C" => "3" },
			 { "A" => "2", "C" => "6" } ] }
    config <<-CONFIG
      filter {
        collection { group_by => { "src" => "_['A']" } }
      }
    CONFIG

    sample event do
      insist { subject.get("src") } === { "1" => [ { "A" => "1", "B" => "2" }, { "A" => "1", "C" => "3" } ],
                                          "2" => [ { "A" => "2", "B" => "4" }, { "A" => "2", "C" => "6" } ] }
    end
  end

  context "group_by: non-matching criteria grouped with empty value" do
    event = { "src" => [ { "A" => "1", "B" => "2" },
                         { "A" => "2", "B" => "4" },
                         { "A" => "1", "C" => "3" },
                         { "A" => "2", "C" => "6" } ] }
    config <<-CONFIG
      filter {
        collection { group_by => { "src" => "_['B']" } }
      }
    CONFIG

    sample event do
      insist { subject.get("src") } === { "2" => [ { "A" => "1", "B" => "2" } ],
                                          "4" => [ { "A" => "2", "B" => "4" } ],
                                          ""  => [ { "A" => "1", "C" => "3" },
                                                   { "A" => "2", "C" => "6" } ] }
    end
  end

  context "group_by: group by function result" do
    event = { "src" => [ 1, 2, 3, 4, 5 ] }
    config <<-CONFIG
      filter {
        collection { group_by => { "src" => "_ > 3" } }
      }
    CONFIG

    sample event do
      insist { subject.get("src") } === { "false" => [ 1, 2, 3 ], "true" => [ 4, 5 ] }
    end
  end

  context "group_by: group values, named" do
    event = { "src" => [ 'A', 'B', 'C', 'A', 'B', 'A' ] }
    config <<-CONFIG
      filter {
        collection { each => "item" group_by => { "src" => "item" } }
      }
    CONFIG

    sample event do
      insist { subject.get("src") } === { "A" => [ "A", "A", "A" ],
                                          "B" => [ "B", "B" ],
                                          "C" => [ "C" ] }
    end
  end

  context "group_by: grouped values written to target field" do
    event = { "src" => [ 'A', 'B', 'C', 'A', 'B', 'A' ] }
    config <<-CONFIG
      filter {
        collection { group_by => { "src" => "_" } target => "dst" }
      }
    CONFIG

    sample event do
      insist { subject.get("src") } === [ 'A', 'B', 'C', 'A', 'B', 'A' ]
      insist { subject.get("dst") } === { "A" => [ "A", "A", "A" ],
                                          "B" => [ "B", "B" ],
                                          "C" => [ "C" ] }
    end
  end

  context "group_by: dereference field names" do
    event = {
      "src" => [ 'A', 'B', 'C', 'A', 'B', 'A' ],
      "deref" => "src",
      "target" => "dst"
    }

    config <<-CONFIG
      filter {
        collection { group_by => { "%{deref}" => "_" } target => "%{target}" }
      }
    CONFIG

    sample event do
      insist { subject.get("src") } === [ 'A', 'B', 'C', 'A', 'B', 'A' ]
      insist { subject.get("dst") } === { "A" => [ "A", "A", "A" ],
                                          "B" => [ "B", "B" ],
                                          "C" => [ "C" ] }
    end
  end

  # === last ===
  context "last: last 0 items from the collection" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { last => { "src" => 0 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq []
    end
  end

  context "last: last n items from the collection" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { last => { "src" => 3 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'C', 'D', 'E' ]
    end
  end

  context "last: all items from the collection" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { last => { "src" => 9 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
    end
  end

  context "last: the value in the source field is not a collection" do
    event = { "src" => "ABCDE" }
    config <<-CONFIG
      filter {
        collection { last => { "src" => 2 } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'ABCDE' ]
    end
  end

  context "last: last n items from the collection are written to target field" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { last => { "src" => 3 } target => "dst" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
      expect(subject.get("dst")).to eq [ 'C', 'D', 'E' ]
    end
  end

  context "last: dereference field names" do
    event = {
      "src" => [ 'A', 'B', 'C', 'D', 'E' ],
      "deref" => "src",
      "target" => "dst"
    }
    config <<-CONFIG
      filter {
        collection { last => { "%{deref}" => 3 } target => "%{target}" }
      }
    CONFIG

    sample event do
      expect(subject.get("dst")).to eq [ 'C', 'D', 'E' ]
    end
  end

  # === map ===
  context "map: apply function to all elements of the collection, unnamed" do
    event = { "src" => [ 1, 2, 3, 4, 5 ] }
    config <<-CONFIG
      filter {
        collection { map => { "src" => "_ * 2" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 2, 4, 6, 8, 10 ]
    end
  end

  context "map: apply function to all elements of the collection, named" do
    event = { "src" => [ 1, 2, 3, 4, 5 ] }
    config <<-CONFIG
      filter {
        collection { each => "item" map => { "src" => "item * 2" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 2, 4, 6, 8, 10 ]
    end
  end

# TEST FOR RAISED ERRORS
#  context "map: nils can be handled?" do
#    event = { "src" => [ 1, 2, nil, 4, 5 ] }
#    config <<-CONFIG
#      filter {
#        collection { map => { "src" => "_ * 2" } }
#      }
#    CONFIG
#
#    sample event do
#      expect(subject.get("src")).to eq [ 2, 4, nil, 8, 10 ]
#    end
#  end

  context "map: mapping on a non collection value will put the resulting value in a collection" do
    event = { "src" => 1 }
    config <<-CONFIG
      filter {
        collection { map => { "src" => "_ * 2" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 2 ]
    end
  end

  context "map: mapping on a nil value will generate an empty collection" do
    event = { "src" => nil }
    config <<-CONFIG
      filter {
        collection { map => { "src" => "_ * 2" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq []
    end
  end

  context "map: collection with function applied written to target field" do
    event = { "src" => [ 1, 2, 3, 4, 5 ] }
    config <<-CONFIG
      filter {
        collection { map => { "src" => "_ * 2" } target => "dst" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 1, 2, 3, 4, 5 ]
      expect(subject.get("dst")).to eq [ 2, 4, 6, 8, 10 ]
    end
  end

  context "map: dereference field names" do
    event = {
      "src" => [ 1, 2, 3, 4, 5 ],
      "deref" => "src",
      "target" => "dst"
    }

    config <<-CONFIG
      filter {
        collection { map => { "%{deref}" => "_ * 2" } target => "%{target}" }
      }
    CONFIG

    sample event do
      expect(subject.get("dst")).to eq [ 2, 4, 6, 8, 10 ]
    end
  end

  # === reverse ===
  context "reverse: reverse all elements of the collection" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { reverse => "src" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'E', 'D', 'C', 'B', 'A' ]
    end
  end

  context "reverse: reverse all elements of the collection at only one level" do
    event = { "src" => [ 'A', [ 'B', [ 'B1', 'B2' ] ], 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { reverse => "src" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'E', 'D', 'C', [ 'B', [ 'B1', 'B2' ] ], 'A' ]
    end
  end

  context "reverse: non-collection value will be put in a collection" do
    event = { "src" => 'ABCDE' }
    config <<-CONFIG
      filter {
        collection { reverse => "src" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'ABCDE' ]
    end
  end

  context "reverse: put the reversed collection to the target field" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { reverse => "src" target => "dst"}
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
      expect(subject.get("dst")).to eq [ 'E', 'D', 'C', 'B', 'A' ]
    end
  end

  context "reverse: dereference field names" do
    event = {
      "src" => [ 'A', 'B', 'C', 'D', 'E' ],
      "deref" => "src",
      "target" => "dst"
    }

    config <<-CONFIG
      filter {
        collection { reverse => "%{deref}" target => "%{target}"}
      }
    CONFIG

    sample event do
      expect(subject.get("dst")).to eq [ 'E', 'D', 'C', 'B', 'A' ]
    end
  end

  # === remove ===
  context "remove: remove only matching members of the collection, unnamed elements" do
    event = { "src" => [ 1, 2, 3, 4, 5 ] }
    config <<-CONFIG
      filter {
        collection { remove => { "src" => "_ >= 4" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 1, 2, 3 ]
    end
  end

  context "remove: remove only matching members of the collection, named elements" do
    event = { "src" => [ 1, 2, 3, 4, 5 ] }
    config <<-CONFIG
      filter {
        collection { each => "item" remove => { "src" => "item >= 4" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 1, 2, 3 ]
    end
  end

  context "remove: remove only matching members of the collection, no matches" do
    event = { "src" => [ 1, 2, 3, 4, 5 ] }
    config <<-CONFIG
      filter {
        collection { remove => { "src" => "_ < 0" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 1, 2, 3, 4, 5 ]
    end
  end

  context "remove: handle nils" do
    event = { "src" => [ 'A', nil, 'B', nil, 'C', nil ] }
    config <<-CONFIG
      filter {
        collection { remove => { "src" => "_.nil?" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C' ]
    end
  end

  context "remove: non-collection value unchanged with nonsense test" do
    event = { "src" => 'ABCDE' }
    config <<-CONFIG
      filter {
        collection { remove => { "src" => "_ < 4" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq 'ABCDE'
    end
  end

  context "remove: non-collection value put in collection when not matched" do
    event = { "src" => 'ABCDE' }
    config <<-CONFIG
      filter {
        collection { remove => { "src" => "_.length < 4" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'ABCDE' ]
    end
  end

  context "remove: empty collection when non-collection value matches" do
    event = { "src" => 'ABCDE' }
    config <<-CONFIG
      filter {
        collection { remove => { "src" => "_.length > 4" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq []
    end
  end

  context "remove: collection of matching elements written to target field" do
    event = { "src" => [ 1, 2, 3, 4, 5 ] }
    config <<-CONFIG
      filter {
        collection { remove => { "src" => "_ >= 4" } target => "dst" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 1, 2, 3, 4, 5 ]
      expect(subject.get("dst")).to eq [ 1, 2, 3 ]
    end
  end

  context "remove: dereference field names" do
    event = {
      "src" => [ 1, 2, 3, 4, 5 ],
      "deref" => "src",
      "target" => "dst"
    }

    config <<-CONFIG
      filter {
        collection { remove => { "%{deref}" => "_ >= 4" } target => "%{target}" }
      }
    CONFIG

    sample event do
      expect(subject.get("dst")).to eq [ 1, 2, 3 ]
    end
  end

  # === select ===
  context "select: filter to only matching members of the collection, unnamed elements" do
    event = { "src" => [ 1, 2, 3, 4, 5 ] }
    config <<-CONFIG
      filter {
        collection { select => { "src" => "_ < 4" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 1, 2, 3 ]
    end
  end

  context "select: filter to only matching members of the collection, named elements" do
    event = { "src" => [ 1, 2, 3, 4, 5 ] }
    config <<-CONFIG
      filter {
        collection { each => "item" select => { "src" => "item < 4" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 1, 2, 3 ]
    end
  end

  context "select: filter to only matching members of the collection, no matches" do
    event = { "src" => [ 1, 2, 3, 4, 5 ] }
    config <<-CONFIG
      filter {
        collection { select => { "src" => "_ < 0" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq []
    end
  end

  context "select: handle nils" do
    event = { "src" => [ 'A', nil, 'B', nil, 'C', nil ] }
    config <<-CONFIG
      filter {
        collection { select => { "src" => "_.nil?" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ nil, nil, nil ]
    end
  end

  context "select: non-collection value unchanged with nonsense test" do
    event = { "src" => 'ABCDE' }
    config <<-CONFIG
      filter {
        collection { select => { "src" => "_ < 4" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq 'ABCDE'
    end
  end

  context "select: empty collection when non-collection value does not match" do
    event = { "src" => 'ABCDE' }
    config <<-CONFIG
      filter {
        collection { select => { "src" => "_.length < 4" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq []
    end
  end

  context "select: non-collection value unchanged when matched put in collection" do
    event = { "src" => 'ABCDE' }
    config <<-CONFIG
      filter {
        collection { select => { "src" => "_.length > 4" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'ABCDE' ]
    end
  end

  context "select: collection of matching elements written to target field" do
    event = { "src" => [ 1, 2, 3, 4, 5 ] }
    config <<-CONFIG
      filter {
        collection { select => { "src" => "_ < 4" } target => "dst" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 1, 2, 3, 4, 5 ]
      expect(subject.get("dst")).to eq [ 1, 2, 3 ]
    end
  end

  context "select: dereference field names" do
    event = {
      "src" => [ 1, 2, 3, 4, 5 ],
      "deref" => "src",
      "target" => "dst"
    }

    config <<-CONFIG
      filter {
        collection { select => { "%{deref}" => "_ < 4" } target => "%{target}" }
      }
    CONFIG

    sample event do
      expect(subject.get("dst")).to eq [ 1, 2, 3 ]
    end
  end

  # === split ===
  context "split: convert delimited string to collection" do
    event = { "src" => 'A,B,C,D,E' }
    config <<-CONFIG
      filter {
        collection { split => { "src" => "," } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
    end
  end

  context "split: empty strings result when no value between delimiters, except for trailing blank" do
    # seems like there should be a trailing ""; but that's not native behaviour from testing, so
    # will leave as is for now
    event = { "src" => ',A,B,,C,,D,E,' }
    config <<-CONFIG
      filter {
        collection { split => { "src" => "," } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ '', 'A', 'B', '', 'C', '', 'D', 'E' ]
    end
  end

  context "split: no delimiter matches will result in single element collection of value" do
    event = { "src" => 'A,B,C,D,E' }
    config <<-CONFIG
      filter {
        collection { split => { "src" => "-" } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A,B,C,D,E' ]
    end
  end

  context "split: existing collections are unaffected" do
    event = { "src" => [ 'A', 'B1,B2', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection { split => { "src" => "," } }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B1,B2', 'C', 'D', 'E' ]
    end
  end

  context "split: new collection written to target field" do
    event = { "src" => 'A,B,C,D,E' }
    config <<-CONFIG
      filter {
        collection { split => { "src" => "," } target => "dst" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq 'A,B,C,D,E'
      expect(subject.get("dst")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
    end
  end

  context "split: dereference field names" do
    event = {
      "src" => 'A,B,C,D,E',
      "deref" => "src",
      "target" => "dst"
    }

    config <<-CONFIG
      filter {
        collection { split => { "%{deref}" => "," } target => "%{target}" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq 'A,B,C,D,E'
      expect(subject.get("dst")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
    end
  end

  # === subfilter ===
  context "subfilter: run filter for each element of the collection" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection {
	  subfilter => {
	    "src" => {
	      collection => {
	        add_field => { "%{_}" => "test" }
	      }
	    }
	  }
	}
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
      expect(subject.get("A")).to eq 'test'
      expect(subject.get("B")).to eq 'test'
      expect(subject.get("C")).to eq 'test'
      expect(subject.get("D")).to eq 'test'
      expect(subject.get("E")).to eq 'test'
    end
  end

  context "subfilter: target field is not used" do
    event = { "src" => [ 'A', 'B', 'C', 'D', 'E' ] }
    config <<-CONFIG
      filter {
        collection {
          subfilter => {
            "src" => {
              collection => {
                add_field => { "%{_}" => "test" }
              }
            }
          }
	  target => "dst"
        }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
      expect(subject.get("A")).to eq 'test'
      expect(subject.get("B")).to eq 'test'
      expect(subject.get("C")).to eq 'test'
      expect(subject.get("D")).to eq 'test'
      expect(subject.get("E")).to eq 'test'
      expect(subject.get("dst")).to eq nil
    end
  end

  context "subfilter: dereferencing field name" do
    event = {
      "src" => [ 'A', 'B', 'C', 'D', 'E' ],
      "deref" => "src"
    }

    config <<-CONFIG
      filter {
        collection {
          subfilter => {
            "%{deref}" => {
              collection => {
                add_field => { "%{_}" => "test" }
              }
            }
          }
        }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
      expect(subject.get("A")).to eq 'test'
      expect(subject.get("B")).to eq 'test'
      expect(subject.get("C")).to eq 'test'
      expect(subject.get("D")).to eq 'test'
      expect(subject.get("E")).to eq 'test'
    end
  end

  # === uniq ===
  context "uniq: remove duplicate values at the top level" do
    event = { "src" => [ 'A', 'B', 'C', 'C', 'D', 'E', 'B', 'A' ] }
    config <<-CONFIG
      filter {
        collection { uniq => "src" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
    end
  end

  context "uniq: remove duplicate values only at the top level" do
    event = { "src" => [ 'A', 'B', [ 'A', 'B', 'C' ], 'C', 'B', 'A', [ 'A', 'B', 'C' ] ] }
    config <<-CONFIG
      filter {
        collection { uniq => "src" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', [ 'A', 'B', 'C' ], 'C' ]
    end
  end

  context "uniq: non-collection value put in collection" do
    event = { "src" => 'ABCDE' }
    config <<-CONFIG
      filter {
        collection { uniq => "src" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'ABCDE' ]
    end
  end

  context "uniq: de-duplicated collection should be written to target field" do
    event = { "src" => [ 'A', 'B', 'C', 'C', 'D', 'E', 'B', 'A' ] }
    config <<-CONFIG
      filter {
        collection { uniq => "src" target => "dst" }
      }
    CONFIG

    sample event do
      expect(subject.get("src")).to eq [ 'A', 'B', 'C', 'C', 'D', 'E', 'B', 'A' ]
      expect(subject.get("dst")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
    end
  end

  context "uniq: dereference field names" do
    event = {
      "src" => [ 'A', 'B', 'C', 'C', 'D', 'E', 'B', 'A' ],
      "deref" => "src",
      "target" => "dst"
    }

    config <<-CONFIG
      filter {
        collection { uniq => "%{deref}" target => "%{target}" }
      }
    CONFIG

    sample event do
      expect(subject.get("dst")).to eq [ 'A', 'B', 'C', 'D', 'E' ]
    end
  end  
end
